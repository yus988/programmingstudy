var express = require('express')
var  bodyParser = require('body-parser')
var app = express();  //expressのmethodを使いやすくするためにappに代入

var urlencodedParser = bodyParser.urlencoded({ extended: false })

app.set('view engine', 'ejs'); //viewerにejsを指定
app.use('/assets',express.static('assets')) //ミドルウェア、staticファイルを取ってくる

app.get('/',function(req,res){
    res.render('index')  //
})

app.get('/contact',function(req,res){
    res.render('contact',{qs: req.query})  //リクエストの末尾にあるクエリの情報を取る。dept=→qs.deptでとれる
})

app.post('/contact', urlencodedParser, function(req,res){
    console.log(req.body);
    res.render('contact-success',{data: req.body})  //contact-successをrenderする際に、変数dataにreq.bodyを代入して渡す
})

app.get('/profile/:name', function(req,res){ 
    var data = {age: 29, job: 'ninja', hobbies: ['eating','fighting','fishing']}
    res.render('profile', {person: req.params.name, data: data})   //profile.ejsの内容を表示、req.params.nameをprofile.ejsに代入
})

// Start the server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
  console.log('Press Ctrl+C to quit.');
});


//app.listen(3000)

/*

----------------------------------------
app.use('/assets',function(req, res, next){
    console.log(req.url)
    next()
})
--------------------------------------
var express = require('express')
var app = express();  //expressのmethodを使いやすくするためにappに代入

app.set('view engine', 'ejs');


app.get('/',function(req,res){
    res.sendFile(__dirname + '/index.html')  //content-typeはexpressによって自動で検出される
})

app.get('/contact',function(req,res){
    res.sendFile(__dirname + '/contact.html')  //content-typeはexpressによって自動で検出される
})

app.get('/profile/:name', function(req,res){ // /profile/の後のidのところに記入された文字が↓のreq.params.idに代入される
    res.send('You requested to see a profile with the id of ' + req.params.name)
})

app.listen(3000)
-------------------------------------------------------
var http = require('http')
var fs = require('fs')

var server = http.createServer(function(req,res){   //リクエストがサーバーに送られたとき、functionが発火する
    console.log('request was made: ' + req.url)
    if(req.url === '/home' || req.url === '/'){
        res.writeHead(200,{'Content-Type': 'text/html'})
        fs.createReadStream(__dirname + '/index.html').pipe(res)
    } else if(req.url === '/contact'){
        res.writeHead(200,{'Content-Type': 'text/html'})
        fs.createReadStream(__dirname + '/contact.html').pipe(res)
    } else if(req.url === '/api/ninjas'){
        var ninjas = [{name: 'ryu', age: 29}, {name: 'yoshi', age: 32}]
        res.writeHead(200, {'Content-Type': 'application/json'})
        res.end(JSON.stringify(ninjas))
    } else{
        res.writeHead(404,{'Content-Type': 'text/html'})
        fs.createReadStream(__dirname + '/404.html').pipe(res)
    }
})

server.listen(3000,'127.0.0.1')  //サーバーを立てて、（）内の変数でリッスンする
console.log('yo dawgs, now listening to port 3000')
--------------------------------
var server = http.createServer(function(req,res){   //リクエストがサーバーに送られたとき、functionが発火する
    console.log('request was made: ' + req.url)
    res.writeHead(200,{'Content-Type': 'application/json'})  //htmlとブラウザに認識させる
    var myObj = {
        name: 'Ryu',
        job: 'Ninja',
        age: 29
    }
    res.end(JSON.stringify(myObj))
---------------------------------------------------
var http = require('http')
var fs = require('fs')

var server = http.createServer(function(req,res){   //リクエストがサーバーに送られたとき、functionが発火する
    console.log('request was made: ' + req.url)
    res.writeHead(200,{'Content-Type': 'text/html'})  //htmlとブラウザに認識させる
    var myReadStream = fs.createReadStream(__dirname + '/index.html', 'utf8') //createReadStreamはEventemitterを継承している 
    myReadStream.pipe(res)
})

server.listen(3000,'127.0.0.1')  //サーバーを立てて、（）内の変数でリッスンする
console.log('yo dawgs, now listening to port 3000')
-------------------------
    var myReadStream = fs.createReadStream(__dirname + '/readMe.txt', 'utf8') //createReadStreamはEventemitterを継承している
    var myWriteStream = fs.createWriteStream(__dirname + '/writeMe.txt')    
    myReadStream.pipe(myWriteStream)
-------------------------
myReadStream.on('data', function(chunk){
    console.log('new chunk recieved: ')
    // console.log(chunk)
    myWriteStream.write(chunk)
})
-----------------------
var http = require('http')

var server = http.createServer(function(req,res){   //リクエストがサーバーに送られたとき、functionが発火する
    console.log('request was made: ' + req.url)
    res.writeHead(200,{'Content-Type': 'text/plain'})
    res.end('Hey ninjas')
})

server.listen(3000,'127.0.0.1')
console.log('yo dawgs, now listening to port 3000')
-----------------------
fs.unlink('./stuff/writeMe.txt', function(){
    fs.rmdirSync('stuff')
})
-----------------------
fs.mkdir('stuff',function(){
    fs.readFile('readMe.txt','utf8',function(err,data){
        fs.writeFileSync('./stuff/writeMe.txt', data)
    })
})
---------------------
fs.readFile('readMe.txt', 'utf8', function(err, data){ //Syncが無い場合、読んでいる最中にも以降の処理が走る
    console.log(data)
})
console.log('test')
----------------
var fs = require('fs');

var readMe = fs.readFileSync('readMe.txt', 'utf8');  //Sync: 完了するまで以下の処理は行われない
fs.writeFileSync('writeMe.txt', readMe);
------------------
var events = require('events');
var util = require('util')

var Person = function(name){
    this.name = name
}

util.inherits(Person, events.EventEmitter)

var james = new Person('james')
var mary = new Person('mary')
var ryu = new Person('ryu')
var people = [james,mary,ryu]

people.forEach(function(person){
    person.on('speak', function(mssg){
        console.log(person.name + ' said' + mssg)
    })
})

james.emit('speak',' hey dudes')
ryu.emit('speak',' I want a curry')

------------
var myEmitter = new events.EventEmitter();

myEmitter.on('someEvent',function(mssg){
    console.log(mssg)
})

myEmitter.emit('someEvent', 'the event was emitted')
-----------
var stuff = require('./stuff.js');

console.log(stuff.counter(['shaun', 'crystal', 'ryu']));
console.log(stuff.adder(5,6))
console.log(stuff.adder(stuff.pi,5))

-----------

function callFunction(fun){
    fun();
}

var sayBye = function(){
    console.log('bye')
};

callFunction(sayBye);
*/